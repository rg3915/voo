from django.urls import include, path
from django.contrib import admin


urlpatterns = [
    path('', include('voos.core.urls', namespace='core')),
    path('accounts/', include('voos.accounts.urls')),  # without namespace
    path('operacao/', include('voos.operacao.urls', namespace='operacao')),
    path('admin/', admin.site.urls),
]
