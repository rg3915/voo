from django.urls import path
from voos.operacao import views as v


app_name = 'operacao'

urlpatterns = [
    path('escala/', v.escala, name='escala'),
]
