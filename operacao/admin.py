from django.contrib import admin
from .models import Voo, Funcionario, Escala

admin.site.register(Escala)
admin.site.register(Funcionario)
admin.site.register(Voo)
