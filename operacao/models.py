from django.db import models


class Escala(models.Model):
    tripulante = models.ForeignKey(
        'Funcionario',
        verbose_name='tripulante',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    voo = models.ForeignKey(
        'Voo',
        verbose_name='voo',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = 'escala'
        verbose_name_plural = 'escalas'

    def __str__(self):
        return f'{self.tripulante.nome} - {self.voo.num_voo}'


class Voo(models.Model):
    data = models.DateField('Data')
    prefixo = models.CharField(max_length=50, null=True, blank=True)
    origem = models.CharField(max_length=50, null=True, blank=True)
    destino = models.CharField(max_length=50, null=True, blank=True)
    num_voo = models.CharField('Nº Voo', max_length=6, null=True, blank=True)
    criado_em = models.DateTimeField('criado em', auto_now_add=True)

    class Meta:
        verbose_name = 'voo'
        verbose_name_plural = 'voos'

    def __str__(self):
        return f'{self.num_voo} / {self.data} / {self.origem} / {self.destino}'


class Funcionario(models.Model):
    matricula = models.CharField('matrícula', max_length=15)
    nome = models.CharField('nome', max_length=100)
    area = models.CharField('área', max_length=100, blank=True)
    data_admissao = models.DateField('data de admissão')
    base = models.CharField('base', max_length=20, blank=True)
    criado_em = models.DateTimeField('criado em', auto_now_add=True)

    class Meta:
        verbose_name = 'funcionário'
        verbose_name_plural = 'funcionários'

    def __str__(self):
        return f'{self.matricula} - {self.nome}'
