from django.shortcuts import render
from .models import Escala


def escala(request):
    template_name = 'escala.html'
    dias = range(1, 32)
    escalas = Escala.objects.all()
    context = {'dias': dias, 'escalas': escalas}
    return render(request, template_name, context)
