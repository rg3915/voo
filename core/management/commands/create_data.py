from django.core.management.base import BaseCommand

from voos.crm.models import Person
from voos.utils import utils as u
from voos.utils.progress_bar import progressbar


class Command(BaseCommand):
    help = 'Create data.'

    def handle(self, *args, **options):
        self.stdout.write('Create data.')
